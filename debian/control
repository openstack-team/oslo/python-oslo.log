Source: python-oslo.log
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Mickael Asseline <mickael@papamica.com>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools (>= 99~),
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 bandit,
 python3-coverage,
 python3-dateutil,
 python3-debtcollector (>= 3.0.0),
 python3-eventlet,
 python3-fixtures,
 python3-openstackdocstheme,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.i18n,
 python3-oslo.serialization,
 python3-oslo.utils,
 python3-oslotest,
 python3-pyasyncore,
 python3-stestr,
 python3-subunit,
 python3-testtools,
 subunit,
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/openstack-team/oslo/python-oslo.log
Vcs-Git: https://salsa.debian.org/openstack-team/oslo/python-oslo.log.git
Homepage: https://github.com/openstack/oslo.log.git

Package: python-oslo.log-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: OpenStack logging configuration library - doc
 OpenStack logging configuration library provides standardized configuration
 for all openstack projects. It also provides custom formatters, handlers and
 support for context specific logging (like resource id's etc).
 .
 This package contains the documentation.

Package: python3-oslo.log
Architecture: all
Depends:
 python3-dateutil,
 python3-debtcollector (>= 3.0.0),
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.i18n,
 python3-oslo.serialization,
 python3-oslo.utils,
 python3-pbr,
 python3-pyasyncore,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-oslo.log-doc,
Description: OpenStack logging configuration library - Python 3.x
 OpenStack logging configuration library provides standardized configuration
 for all openstack projects. It also provides custom formatters, handlers and
 support for context specific logging (like resource id's etc).
 .
 This package contains the Python 3.x module.
